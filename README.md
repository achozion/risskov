## This is Booking test Application for Risskov

## Init

1. build and start the Docker container
2. Make database and create tables
3. import bookings.csv and capacity.csv data into the tables. Use JetBrains dataGrip import function. (or other import software)
4. you can reach the application in that route: http://localhost/bookings/

## during implementation 
pucrhase price = cost price because there aren't cost price in bookings CSV file

## Docker 
check running containers: 
docker ps

docker build: 
docker-compose up -d --build

docker run container: 
docker-compose up -d

docker kill other containers: 
docker kill -comtainername-

enable port 80 for the app

## database commands

docker-compose exec db bash

mysql -u root -p

show databases;

GRANT ALL ON laravel.* TO 'laraveluser'@'%' IDENTIFIED BY 'your_laravel_db_password';
FLUSH PRIVILEGES;
EXIT;
    
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost' WITH GRANT OPTION;

## Put it in .env file:

DB_CONNECTION=mysql

DB_HOST=db

DB_PORT=3306

DB_DATABASE=laravel

DB_USERNAME=root

DB_PASSWORD=asdasd123

## Laravel Commands:

docker-compose exec app php artisan route:list

docker-compose exec app composer dump-autoload

for clear caches:

docker-compose exec app php artisan route:cache

docker-compose exec app php artisan config:cache

migration:

docker-compose exec app php artisan make:migration create_bookings_table

docker-compose exec app php artisan make:migration create_capacities_table

docker-compose exec app php artisan migrate

docker-compose exec app php artisan tinker

>>> \DB::table('migrations')->get();








