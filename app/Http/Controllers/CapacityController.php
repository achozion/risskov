<?php

namespace App\Http\Controllers;

use App\Models\Capacities;
use App\Models\Originals;
use App\Services\CapacityService;
use Illuminate\Http\Request;

class CapacityController extends Controller
{
    /**
     * @var CapacityService
     */
    protected $capacityService;

    /**
     * CapacityController constructor.
     * @param CapacityService $capacityService
     */
    public function __construct(CapacityService $capacityService)
    {
        $this->capacityService = $capacityService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $capacity = Capacities::all();

        return view('viewcapacity', ['allCapacity' => $capacity]);
    }

    /**
     * Display a listing of the original resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listOriginal()
    {
        $originals = Originals::all();

        return view('viewcapacity', ['allCapacity' => $originals]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function clarify()
    {
        $this->capacityService->clarify();
        $capacity = Capacities::all();

        return view('viewcapacity', ['allCapacity' => $capacity]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
