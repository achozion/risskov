<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\DataProviderService;

class ReservationController extends Controller
{
    /**
     * @var DataProviderService
     */
    protected $dataProviderService;

    const maxPeopleNumPerReservation = 2;

    /**
     * ReservationController constructor.
     * @param DataProviderService $dataProviderService
     */
    public function __construct(DataProviderService $dataProviderService)
    {
        $this->dataProviderService = $dataProviderService;
    }

    /**
     * reservationSend
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reservationSend(Request $request)
    {
        $customer_id1 = $request->input('customer_id1');
        $hotel_id1 = $request->input('hotel_id1');
        $arrival_date1 = $request->input('arrival_date1');
        $nights1 = $request->input('nights1');

        $customer_id2 = $request->input('customer_id2');
        $hotel_id2 = $request->input('hotel_id2');
        $arrival_date2 = $request->input('arrival_date2');
        $nights2 = $request->input('nights2');

        $maxPeopleNumPerReservationPost = $request->input('maxPeopleNumPerReservationPost');

        $customerIds = $this->dataProviderService->getCustomerIds();
        $hotelIds = $this->dataProviderService->getHotelIds();

        return view('reservation',
            [
                'customerIds' => $customerIds,
                'hotelIds' => $hotelIds,
                'maxPeopleNumPerReservation' => self::maxPeopleNumPerReservation,
                'maxPeopleNumPerReservationPost' => $maxPeopleNumPerReservationPost,

                'customer_id1' => $customer_id1,
                'hotel_id1' => $hotel_id1,
                'arrival_date1' => $arrival_date1,
                'nights1' => $nights1,

                'customer_id2' => $customer_id2,
                'hotel_id2' => $hotel_id2,
                'arrival_date2' => $arrival_date2,
                'nights2' => $nights2,
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reservation()
    {
        $customerIds = $this->dataProviderService->getCustomerIds();
        $hotelIds = $this->dataProviderService->getHotelIds();

        return view('reservation',
            [
                'customerIds' => $customerIds,
                'hotelIds' => $hotelIds,
                'maxPeopleNumPerReservation' => self::maxPeopleNumPerReservation,
                'maxPeopleNumPerReservationPost' => 1,

                'customer_id1' => 1,
                'hotel_id1' => 1,
                'arrival_date1' => "",
                'nights1' => 1,

                'customer_id2' => 1,
                'hotel_id2' => 1,
                'arrival_date2' => "",
                'nights2' => 1,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
