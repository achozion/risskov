<?php

namespace App\Http\Controllers;

use App\Models\Bookings;
use App\Services\BookingService;
use Illuminate\Http\Request;

class BookingsController extends Controller
{
    /**
     * @var BookingService
     */
    protected $bookingService;

    /**
     * BookingsController constructor.
     * @param BookingService $bookingService
     */
    public function __construct(BookingService $bookingService)
    {
        $this->bookingService = $bookingService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = Bookings::all();

        return view('viewbookings', ['allBookings' => $bookings]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function smallestWeekendStays()
    {
        $smallestWeekendStays = $this->bookingService->calculateSmallestWeekendStays();

        return view('viewsmallestweekendstays', ['allSmallestWeekendStays' => $smallestWeekendStays]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function averageRejection()
    {
        $averageRejection = $this->bookingService->calculateAverageRejection();

        return view('viewaveragerejection', ['allAverageRejection' => $averageRejection]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function biggestProfitWeek()
    {
        $biggestProfitWeek = $this->bookingService->calculateBiggestProfitWeek();

        return view('viewbiggestprofitweek', ['allBiggestProfitWeek' => $biggestProfitWeek]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mostUnluckyCustomers()
    {
        $mostUnluckyCustomers = $this->bookingService->calculateMostUnluckyCustomers();

        return view('viewmostunluckycustomer', ['allMostUnluckyCustomers' => $mostUnluckyCustomers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
