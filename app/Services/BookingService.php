<?php

namespace App\Services;

use App\Models\Bookings;
use App\Models\Capacities;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BookingService
{
    /**
     * calculateSmallestWeekendStays
     *
     * return $weekendStaysCount
     */
    public function calculateSmallestWeekendStays()
    {
        $bookings = Bookings::all()->sortByDesc("id")->map->only(['id','hotel_id', 'arrival_date', 'nights']);

        $weekendStaysCount = [];
        Carbon::setWeekendDays([Carbon::FRIDAY, Carbon::SATURDAY]);
        foreach ($bookings as $booking) {
            for ($i=0;$i<(integer)$booking['nights'];$i++) {
                $date = Carbon::parse($booking['arrival_date'])->addDays($i);
                if ($date->isWeekend()) {
                    if (array_key_exists($booking['hotel_id'], $weekendStaysCount)) {
                        $weekendStaysCount[$booking['hotel_id']] = $weekendStaysCount[$booking['hotel_id']] + 1;
                    } else {
                        $weekendStaysCount[$booking['hotel_id']] = 1;
                    }
                }
            }
        }

        asort($weekendStaysCount);
        return $weekendStaysCount;
    }

    /**
     * calculateSmallestWeekendStays
     *
     * return array averageRejectionPerHotel
     */
    public function calculateAverageRejection()
    {
        $allHotelIds = DB::table('bookings')->select("hotel_id")->groupBy('hotel_id')->get();
        $hotelIdsArray = array();
        foreach ($allHotelIds as $hotelIds) {
            $hotelIdsArray[] = $hotelIds->hotel_id;
        }

        $eachBookingRequests = array();
        $eachHotelCapacities = array();
        $eachRequestsCountForHotels = array();
        $eachRejectedRequestsCountForHotels = array();
        $eachDateAndCapacityForHotel = array();
        foreach ($hotelIdsArray as $hotelId) {
            $eachRejectedRequestsCountForHotels[$hotelId] = 0;
            $eachHotelCapacities[$hotelId] = Capacities::where("hotel_id", $hotelId)->get();

            foreach ($eachHotelCapacities[$hotelId]  as $capacity) {
                $eachDateAndCapacityForHotel[$hotelId][$capacity["date"]] = $capacity["capacity"];
            }
        }

        foreach ($hotelIdsArray as $hotelId) {
            $eachBookingRequests[$hotelId] = Bookings::where("hotel_id", $hotelId)->orderBy("arrival_date")->get();

            $eachRequestsCountForHotels[$hotelId] = 0;
            foreach ($eachBookingRequests[$hotelId] as $bookingRequest) {
                $eachRequestsCountForHotels[$hotelId]++;
                $requestedDates = array();
                for ($i=0;$i<(integer)$bookingRequest['nights'];$i++) {
                    $requestedDates[] = Carbon::parse($bookingRequest['arrival_date'])->addDays($i)->format('Y-m-d');;
                }

                $isCapacitiesForAllDates = true;
                foreach ($requestedDates as $date) {
                    if (isset($eachDateAndCapacityForHotel[$hotelId][$date]) && $eachDateAndCapacityForHotel[$hotelId][$date] < 1) {
                        $isCapacitiesForAllDates = false;
                    }
                }

                //request rejected
                if ($isCapacitiesForAllDates === false) {
                    if (isset($eachRejectedRequestsCountForHotels[$hotelId])) {
                        $eachRejectedRequestsCountForHotels[$hotelId]++;
                    } else {
                        $eachRejectedRequestsCountForHotels[$hotelId] = 1;
                    }
                }

                //request accepted
                if ($isCapacitiesForAllDates === true) {
                    foreach ($requestedDates as $date) {
                        if (isset($eachDateAndCapacityForHotel[$hotelId][$date])) {
                            $eachDateAndCapacityForHotel[$hotelId][$date]--;
                        } else {
                            $eachDateAndCapacityForHotel[$hotelId][$date] = 0;
                        }
                    }
                }
            }
        }

        $averageRejectionPerHotel = array();
        foreach ($hotelIdsArray as $hotelId) {
            $averageRejectionPerHotel[$hotelId] = array(
                "rejected" => $eachRejectedRequestsCountForHotels[$hotelId],
                "allrequests" => $eachRequestsCountForHotels[$hotelId],
                "percentage" => round(($eachRejectedRequestsCountForHotels[$hotelId] / $eachRequestsCountForHotels[$hotelId]) * 100, 2)
            );
        }

        return $averageRejectionPerHotel;
    }

    /**
     * calculateMostUnluckyCustomers
     *
     * return array $rejectedCustomerIdsCount
     */
    public function calculateMostUnluckyCustomers()
    {
        $allHotelIds = DB::table('bookings')->select("hotel_id")->groupBy('hotel_id')->get();
        $hotelIdsArray = array();
        foreach ($allHotelIds as $hotelIds) {
            $hotelIdsArray[] = $hotelIds->hotel_id;
        }

        $eachBookingRequests = array();
        $eachHotelCapacities = array();
        $eachRejectedRequestsCountForHotels = array();
        $eachDateAndCapacityForHotel = array();
        $rejectedCustomerIdsCount = array();

        foreach ($hotelIdsArray as $hotelId) {
            $eachRejectedRequestsCountForHotels[$hotelId] = 0;
            $eachHotelCapacities[$hotelId] = Capacities::where("hotel_id", $hotelId)->get();

            foreach ($eachHotelCapacities[$hotelId]  as $capacity) {
                $eachDateAndCapacityForHotel[$hotelId][$capacity["date"]] = $capacity["capacity"];
            }
        }

        foreach ($hotelIdsArray as $hotelId) {
            $eachBookingRequests[$hotelId] = Bookings::where("hotel_id", $hotelId)->orderBy("arrival_date")->get();

            foreach ($eachBookingRequests[$hotelId] as $bookingRequest) {
                $requestedDates = array();
                for ($i=0;$i<(integer)$bookingRequest['nights'];$i++) {
                    $requestedDates[] = Carbon::parse($bookingRequest['arrival_date'])->addDays($i)->format('Y-m-d');;
                }

                $isCapacitiesForAllDates = true;
                foreach ($requestedDates as $date) {
                    if (isset($eachDateAndCapacityForHotel[$hotelId][$date]) && $eachDateAndCapacityForHotel[$hotelId][$date] < 1) {
                        $isCapacitiesForAllDates = false;
                    }
                }

                //request rejected
                if ($isCapacitiesForAllDates === false) {
                    if (isset($rejectedCustomerIdsCount[$bookingRequest['customer_id']])) {
                        $rejectedCustomerIdsCount[$bookingRequest['customer_id']]++;
                    } else {
                        $rejectedCustomerIdsCount[$bookingRequest['customer_id']] = 1;
                    }
                }

                //request accepted
                if ($isCapacitiesForAllDates === true) {
                    foreach ($requestedDates as $date) {
                        if (isset($eachDateAndCapacityForHotel[$hotelId][$date])) {
                            $eachDateAndCapacityForHotel[$hotelId][$date]--;
                        } else {
                            $eachDateAndCapacityForHotel[$hotelId][$date] = 0;
                        }
                    }
                }
            }
        }

        asort($rejectedCustomerIdsCount);
        return array_reverse($rejectedCustomerIdsCount, true);
    }

    /**
     * calculateBiggestProfitWeek
     *
     * return array $biggestProfitWeekDateAndPrice
     */
    public function calculateBiggestProfitWeek()
    {
        $allHotelIds = DB::table('bookings')->select("hotel_id")->groupBy('hotel_id')->get();
        $hotelIdsArray = array();
        foreach ($allHotelIds as $hotelIds) {
            $hotelIdsArray[] = $hotelIds->hotel_id;
        }

        $eachBookingRequests = array();
        $eachHotelCapacities = array();
        $eachRejectedRequestsCountForHotels = array();
        $eachDateAndCapacityForHotel = array();
        $eachDateAndPrices = array();
        $biggestProfitWeekDateAndPrice = array();
        $priceSortHelper = array();

        foreach ($hotelIdsArray as $hotelId) {
            $eachRejectedRequestsCountForHotels[$hotelId] = 0;
            $eachHotelCapacities[$hotelId] = Capacities::where("hotel_id", $hotelId)->get();

            foreach ($eachHotelCapacities[$hotelId]  as $capacity) {
                $eachDateAndCapacityForHotel[$hotelId][$capacity["date"]] = $capacity["capacity"];
            }
        }

        foreach ($hotelIdsArray as $hotelId) {
            $eachBookingRequests[$hotelId] = Bookings::where("hotel_id", $hotelId)->orderBy("arrival_date")->get();

            foreach ($eachBookingRequests[$hotelId] as $bookingRequest) {
                $requestedDates = array();
                for ($i=0;$i<(integer)$bookingRequest['nights'];$i++) {
                    $requestedDates[] = Carbon::parse($bookingRequest['arrival_date'])->addDays($i)->format('Y-m-d');
                }

                $isCapacitiesForAllDates = true;
                foreach ($requestedDates as $date) {
                    if (isset($eachDateAndCapacityForHotel[$hotelId][$date]) && $eachDateAndCapacityForHotel[$hotelId][$date] < 1) {
                        $isCapacitiesForAllDates = false;
                    }
                }

                //request accepted
                if ($isCapacitiesForAllDates === true) {
                    $eachDateAndPrices[$bookingRequest['arrival_date']] = $bookingRequest['sales_price'] - $bookingRequest['pusrcahse_price'];
                    foreach ($requestedDates as $date) {
                        if (isset($eachDateAndCapacityForHotel[$hotelId][$date])) {
                            $eachDateAndCapacityForHotel[$hotelId][$date]--;
                        } else {
                            $eachDateAndCapacityForHotel[$hotelId][$date] = 0;
                        }
                    }
                }
            }
        }

        $sumPrice = 0;
        foreach ($eachDateAndPrices as $date => $price) {
            $sumPrice += $price;
            $carbonDate = Carbon::parse($date);
            if($carbonDate->dayOfWeek == Carbon::MONDAY) {
                $sumPrice = 0;
            }

            $monday = $carbonDate->startOfWeek()->format('Y-m-d');
            $sunday = $carbonDate->endOfWeek()->format('Y-m-d');
            $priceSortHelper[$monday."_".$sunday] = $sumPrice;
            $biggestProfitWeekDateAndPrice[$monday."_".$sunday] = array(
                "monday" => $monday,
                "sunday" => $sunday,
                "sumPrice" => $sumPrice,
            );
        }

        array_multisort($priceSortHelper, SORT_DESC, $biggestProfitWeekDateAndPrice);
        return $biggestProfitWeekDateAndPrice;
    }
}
