<?php

namespace App\Services;

use App\Models\Bookings;

class DataProviderService
{
    /**
     * provide customer ids
     */
    public function getCustomerIds()
    {
        $customerIds = Bookings::select("customer_id")
            ->orderBy('customer_id')
            ->groupby('customer_id')
            ->get();

        return $customerIds;
    }

    /**
     * provide hotel ids
     */
    public function getHotelIds()
    {
        $hotelIds = Bookings::select("hotel_id")
            ->orderBy('hotel_id')
            ->groupby('hotel_id')
            ->get();

        return $hotelIds;
    }

}
