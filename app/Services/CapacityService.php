<?php

namespace App\Services;

use App\Models\Capacities;

class CapacityService
{
    /**
     * Clean capacity dataset for duplicates (hotel id + date can occur multiple times: keep only the latest one)
     *
     * return array $uniqueCapacity
     */
    public function clarify()
    {
        $allCapacity = Capacities::all()->sortByDesc("id")->map->only(['id','hotel_id', 'date']);

        $uniqueCapacity = [];
        foreach ($allCapacity as $capacity) {
            if (in_array($capacity['hotel_id'] . "," . $capacity['date'], $uniqueCapacity)) {
                Capacities::where('id', $capacity['id'])->delete();
            } else {
                array_push($uniqueCapacity, $capacity['hotel_id'] . "," . $capacity['date']);
            }
        }

        return $uniqueCapacity;
    }
}
