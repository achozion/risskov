<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'hotel_id',
        'customer_id',
        'sales_price',
        'purchase_price',
        'arrival_date',
        'purchase_day',
        'nights',
    ];
}
