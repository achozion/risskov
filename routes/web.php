<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/bookings', 'App\Http\Controllers\BookingsController@index');
Route::get('/capacity/list', 'App\Http\Controllers\CapacityController@list');
Route::get('/capacity/listOriginal', 'App\Http\Controllers\CapacityController@listOriginal');
Route::get('/capacity/clarify', 'App\Http\Controllers\CapacityController@clarify');
Route::get('/bookings/smallestWeekendStays', 'App\Http\Controllers\BookingsController@smallestWeekendStays');
Route::get('/bookings/averageRejection', 'App\Http\Controllers\BookingsController@averageRejection');
Route::get('/bookings/biggestProfitWeek', 'App\Http\Controllers\BookingsController@biggestProfitWeek');
Route::get('/bookings/mostUnluckyCustomers', 'App\Http\Controllers\BookingsController@mostUnluckyCustomers');
Route::get('/reservation', 'App\Http\Controllers\ReservationController@reservation');
Route::post('/reservationSend', 'App\Http\Controllers\ReservationController@reservationSend');
