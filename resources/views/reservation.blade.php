<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>View Bookings | Bookings Store</title>
</head>
<body>

<!DOCTYPE html>
<html>
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
    select { width:150px; height: 40px}
    input { width:150px; height: 40px}
    .hidethis {
        display: none;
    }
</style>
<body class="w3-light-grey w3-content" style="max-width:1600px">

@include('sidebarmenu')

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px">

    <header id="booking">
        <a href="#"><img src="/w3images/avatar_g2.jpg" style="width:65px;" class="w3-circle w3-right w3-margin w3-hide-large w3-hover-opacity"></a>
        <span class="w3-button w3-hide-large w3-xxlarge w3-hover-text-grey" onclick="w3_open()"><i class="fa fa-bars"></i></span>
        <div class="w3-container">
            <h1><b>Booking test page</b></h1>
            <div class="w3-section w3-bottombar w3-padding-16">

            </div>
        </div>
    </header>

    <!-- Contact Section -->
    <div class="w3-container w3-padding-large w3-grey">
        <h4 id="contact"><b>RESERVATION</b></h4>
        <div class="w3-row-padding w3-center w3-padding-24" style="margin:0 -16px">
            <div class="w3-third w3-dark-grey">
                <p><i class="fa fa-envelope w3-xxlarge w3-text-light-grey"></i></p>
                <p>email@email.com</p>
            </div>
            <div class="w3-third w3-teal">
                <p><i class="fa fa-map-marker w3-xxlarge w3-text-light-grey"></i></p>
                <p>Risskov - Denmark, Switzerland </p>
            </div>
            <div class="w3-third w3-dark-grey">
                <p><i class="fa fa-phone w3-xxlarge w3-text-light-grey"></i></p>
                <p>512312311</p>
            </div>
        </div>

        <form action="/reservationSend" method="post">
            @csrf

            <hr class="w3-opacity">
                <div class="w3-section">
                    <h3><label>How many person: </label>

                        <select name="maxPeopleNumPerReservationPost" id="maxPeopleNumPerReservationPost">
                            @for ($i = 1; $i <= $maxPeopleNumPerReservation; $i++)
                                <option value="{{ $i }}" {{ $maxPeopleNumPerReservationPost == $i ? 'selected' : '' }}>{{ $i }}</option>
                            @endfor
                        </select></h3>
                </div>
            <hr class="w3-opacity">

            <div class="w3-row-padding w3-center w3-padding-24" style="margin:0 -16px">
                <div class="w3-quarter">
                    <h3><label>Customer Id</label>

                    <select name="customer_id1" id="customer_id1">
                        @foreach ($customerIds as $customerId)
                            <option value="{{ $customerId->customer_id }}" {{ $customer_id1 == $customerId->customer_id ? 'selected' : '' }}>{{ $customerId->customer_id }}</option>
                        @endforeach
                    </select>
                    </h3>
                </div>

                <div class="w3-quarter">
                    <h3><label>Hotel Id</label>

                    <select name="hotel_id1" id="hotel_id1">
                        @foreach ($hotelIds as $hotelId)
                            <option value="{{ $hotelId->hotel_id }}" {{ $hotel_id1 == $hotelId->hotel_id ? 'selected' : '' }}>{{ $hotelId->hotel_id }}</option>
                        @endforeach
                    </select>
                    </h3>
                </div>

                <div class="w3-quarter">
                    <h3><label>Arrival Date: </label>

                    <input class="w3-border datepicker" type="text" name="arrival_date1" id="arrival_date1" value="{{ $arrival_date1 ? $arrival_date1 : '' }}" required>
                    </h3>
                </div>

                <div class="w3-quarter">
                    <h3><label>Nights: </label>

                    <select name="nights1" id="nights1">
                        @for ($i = 1; $i <= 31; $i++)
                            <option value="{{ $i }}" {{ $nights1 == $i ? 'selected' : '' }}>{{ $i }}</option>
                        @endfor
                    </select>
                    </h3>
                </div>
            </div>

            <hr class="w3-opacity">

            <div class="w3-row-padding w3-center w3-padding-24 hidethis" style="margin:0 -16px">
                <div class="w3-quarter">
                    <h3><label>Customer Id</label>

                    <select name="customer_id2" id="customer_id2">
                        @foreach ($customerIds as $customerId)
                            <option value="{{ $customerId->customer_id }}" {{ $customer_id2 == $customerId->customer_id ? 'selected' : '' }}>{{ $customerId->customer_id }}</option>
                        @endforeach
                    </select>
                    </h3>
                </div>

                <div class="w3-quarter">
                    <h3><label>Hotel Id</label>

                    <select name="hotel_id2" id="hotel_id2">
                        @foreach ($hotelIds as $hotelId)
                            <option value="{{ $hotelId->hotel_id }}" {{ $hotel_id2 == $hotelId->hotel_id ? 'selected' : '' }}>{{ $hotelId->hotel_id }}</option>
                        @endforeach
                    </select>
                    </h3>
                </div>

                <div class="w3-quarter">
                    <h3><label>Arrival Date: </label>

                    <input class="w3-border datepicker" type="text" name="arrival_date2" id="arrival_date2" value="{{ $arrival_date2 ? $arrival_date2 : '' }}">
                    </h3>
                </div>

                <div class="w3-quarter">
                    <h3><label>Nights: </label>

                    <select name="nights2" id="nights2">
                        @for ($i = 1; $i <= 31; $i++)
                            <option value="{{ $i }}" {{ $nights2 == $i ? 'selected' : '' }}>{{ $i }}</option>
                        @endfor
                    </select>
                    </h3>
                </div>
            </div>

            <button type="submit" class="w3-button w3-black w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Send Reservation</button>
        </form>
    </div>

    @include('footer')

    <!-- End page content -->
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script>
    jQuery(document).ready(function(){
        var num = jQuery('#maxPeopleNumPerReservationPost').val();
        if (num == 1) {
            jQuery('.hidethis').hide();
        } else {
            jQuery('.hidethis').show();
        }

        jQuery('.datepicker').datepicker({
            dateFormat: 'yy-mm-dd',
            startDate: '0',
            minDate: 0,
        });

        jQuery('#maxPeopleNumPerReservationPost').change(function () {
            var num = $( this ).val();
            if (num == 1) {
                jQuery('.hidethis').hide();
            } else {
                jQuery('.hidethis').show();
            }
        });
    });

    // Script to open and close sidebar
    function w3_open() {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";
    }

    function w3_close() {
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("myOverlay").style.display = "none";
    }
</script>

</body>
</html>

</body>
</html>
