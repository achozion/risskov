
    <!-- Header -->
    <header id="portfolio">
        <a href="#"><img src="/w3images/avatar_g2.jpg" style="width:65px;" class="w3-circle w3-right w3-margin w3-hide-large w3-hover-opacity"></a>
        <span class="w3-button w3-hide-large w3-xxlarge w3-hover-text-grey" onclick="w3_open()"><i class="fa fa-bars"></i></span>
        <div class="w3-container">
            <h1><b>Booking test page</b></h1>
            <div class="w3-section w3-bottombar w3-padding-16">
                <span class="w3-margin-right">Filter:</span>
                <a href="/bookings"><button class="w3-button w3-black">ALL</button></a>
                <a href="/capacity/list"><button class="w3-button w3-white"><i class="fa fa-diamond w3-margin-right"></i>Clean Capacity Dataset</button></a>
                <a href="/bookings/smallestWeekendStays"><button class="w3-button w3-white"><i class="fa fa-diamond w3-margin-right"></i>Smallest weekend stays</button></a>
                <a href="/bookings/averageRejection"><button class="w3-button w3-white"><i class="fa fa-diamond w3-margin-right"></i>Average rejection</button></a>
                <a href="/bookings/biggestProfitWeek"><button class="w3-button w3-white"><i class="fa fa-diamond w3-margin-right"></i>Biggest profit week</button></a>
                <a href="/bookings/mostUnluckyCustomers"><button class="w3-button w3-white"><i class="fa fa-diamond w3-margin-right"></i>Most unlucky customers</button></a>
            </div>
        </div>
    </header>
