<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>View Bookings | Bookings Store</title>
</head>
<body>

<!DOCTYPE html>
<html>
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
</style>
<body class="w3-light-grey w3-content" style="max-width:1600px">

@include('sidebarmenu')

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px">

    @include('header')

    <div class="w3-row-padding">
        @foreach ($allBookings as $bookings)
            <div class="w3-third w3-container">
                <img src="/w3images/mountains.jpg" alt="" style="width:100%" class="w3-hover-opacity">
                <div class="w3-container w3-white">
                    <p><b>Hotel id: {{ $bookings->hotel_id }}    &nbsp;&nbsp;&nbsp;&nbsp; Customer id: {{ $bookings->customer_id }}</b></p>
                    <p>Arrival date: {{ $bookings->arrival_date }}  &nbsp;&nbsp; nights: {{ $bookings->nights }}.</p>
                </div>
            </div>
        @endforeach
    </div>

{{--    @include('pagination')--}}

    @include('footer')

    <!-- End page content -->
</div>

<script>
    // Script to open and close sidebar
    function w3_open() {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";
    }

    function w3_close() {
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("myOverlay").style.display = "none";
    }
</script>

</body>
</html>

</body>
</html>
