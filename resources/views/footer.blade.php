
    <!-- Footer -->
    <footer class="w3-container w3-padding-32 w3-dark-grey">
        <div class="w3-row-padding">
            <div class="w3-third">
                <h3>FOOTER</h3>
                <p>Praesent tincidunt sed tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.</p>
                <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">achozion@gmail.com</a></p>
            </div>

            <div class="w3-third">
                <h3>FAQ</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="/w3images/workshop.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">Lorem</span><br>
                        <span>Sed mattis nunc</span>
                    </li>
                    <li class="w3-padding-16">
                        <img src="/w3images/gondol.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">Ipsum</span><br>
                        <span>Praes tinci sed</span>
                    </li>
                </ul>
            </div>

            <div class="w3-third">
                <h3>POPULAR TAGS</h3>
                <p>
                    <span class="w3-tag w3-black w3-margin-bottom">Booking</span> <span class="w3-tag w3-grey w3-small w3-margin-bottom">Capacity</span> <span class="w3-tag w3-grey w3-small w3-margin-bottom">Travel</span>
                    <span class="w3-tag w3-grey w3-small w3-margin-bottom">Risskov</span> <span class="w3-tag w3-grey w3-small w3-margin-bottom">Denmark</span> <span class="w3-tag w3-grey w3-small w3-margin-bottom">Switzerland</span>
                </p>
            </div>

        </div>
    </footer>

    <div class="w3-black w3-center w3-padding-24">Powered by <a href="https://www.w3schools.com/w3css/default.asp" title="W3.CSS" target="_blank" class="w3-hover-opacity">achozion@gmail.com</a></div>


